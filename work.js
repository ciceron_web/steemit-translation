
var user = {
	account: "",
	locale: "en"
}
var l10n = {
	"en": {
		"Translate": "Translate",
		"SourceLanguage": "Source language",
		"TargetLanguage": "Target language",
		"ExpectedCost": "Expected cost",
		"Next": "Next",
		"languageKeyAndName": {
			"ar": "Arabic",
			"bg": "Bulgarian",
			"ca": "Catalan",
			"zh-cn": "Chinese (Simplified)",
			"zh-tw": "Chinese (Traditional)",
			"hr": "Croatian",
			"cs": "Czech",
			"da": "Danish",
			"nl": "Dutch",
			"en": "English",
			"et": "Estonian",
			"tl": "Filipino",
			"fi": "Finnish",
			"fr": "French",
			"de": "German",
			"el": "Greek",
			"iw": "Hebrew",
			"hi": "Hindi",
			"hu": "Hungarian",
			"is": "Icelandic",
			"id": "Indonesian",
			"it": "Italian",
			"ja": "Japanese",
			"ko": "Korean",
			"lv": "Latvian",
			"lt": "Lithuanian",
			"no": "Norwegian",
			"pl": "Polish",
			"pt": "Portuguese",
			"ro": "Romanian",
			"ru": "Russian",
			"sr": "Serbian",
			"sk": "Slovak",
			"sl": "Slovenian",
			"es": "Spanish",
			"sv": "Swedish",
			"th": "Thai",
			"tr": "Turkish",
			"uk": "Ukrainian",
			"ur": "Urdu",
			"vi": "Vietnamese"
		}
	},
	"ko": {
		"Translate": "번역하기",
		"SourceLanguage": "원문 언어",
		"TargetLanguage": "번역 언어",
		"ExpectedCost": "예상 비용",
		"Next": "다음",
		"languageKeyAndName": {
			"ar": "Arabic",
			"bg": "Bulgarian",
			"ca": "Catalan",
			"zh-cn": "중국어(간체)",
			"zh-tw": "중국어(번체)",
			"hr": "Croatian",
			"cs": "Czech",
			"da": "Danish",
			"nl": "Dutch",
			"en": "영어",
			"et": "Estonian",
			"tl": "Filipino",
			"fi": "Finnish",
			"fr": "프랑스어",
			"de": "독일어",
			"el": "그리스어",
			"iw": "Hebrew",
			"hi": "Hindi",
			"hu": "Hungarian",
			"is": "Icelandic",
			"id": "Indonesian",
			"it": "이탈리아어",
			"ja": "일본어",
			"ko": "한국어",
			"lv": "Latvian",
			"lt": "Lithuanian",
			"no": "Norwegian",
			"pl": "Polish",
			"pt": "포르투갈어",
			"ro": "Romanian",
			"ru": "러시아어",
			"sr": "Serbian",
			"sk": "Slovak",
			"sl": "Slovenian",
			"es": "스페인어",
			"sv": "스웨덴어",
			"th": "태국어",
			"tr": "터키어",
			"uk": "Ukrainian",
			"ur": "Urdu",
			"vi": "베트남어"
		}
	}
}

var url = "https://st.ciceron.me";

var languageKeyAndName = {
	"ar": "Arabic",
	"bg": "Bulgarian",
	"ca": "Catalan",
	"zh-cn": "중국어(간체)",
	"zh-tw": "중국어(번체)",
	"hr": "Croatian",
	"cs": "Czech",
	"da": "Danish",
	"nl": "Dutch",
	"en": "영어",
	"et": "Estonian",
	"tl": "Filipino",
	"fi": "Finnish",
	"fr": "프랑스어",
	"de": "독일어",
	"el": "그리스어",
	"iw": "Hebrew",
	"hi": "Hindi",
	"hu": "Hungarian",
	"is": "Icelandic",
	"id": "Indonesian",
	"it": "이탈리아어",
	"ja": "일본어",
	"ko": "한국어",
	"lv": "Latvian",
	"lt": "Lithuanian",
	"no": "Norwegian",
	"pl": "Polish",
	"pt": "포르투갈어",
	"ro": "Romanian",
	"ru": "러시아어",
	"sr": "Serbian",
	"sk": "Slovak",
	"sl": "Slovenian",
	"es": "스페인어",
	"sv": "스웨덴어",
	"th": "태국어",
	"tr": "터키어",
	"uk": "Ukrainian",
	"ur": "Urdu",
	"vi": "베트남어"
}

/**
 * 
 * @param {String} key
 * @returns {String} 
 * Name of language 
 * 
 */
var getLanguageNameFromKey = function (key) {
	return l10n[user.locale].languageKeyAndName[key.toLowerCase()] || "알 수 없음";
}



//<img src="${chrome.extension.getURL('icons/icon.png')}" />
// href="${chrome.extension.getURL('request.html')}"
var nodeInsertedCallback = function (e) {
	// var footers = $(".articles__summary-footer");
	var articles = $(".articles__summary");
	articles.push($(".PostFull__header")[0]);
	// footers.push($(".PostFull__footer .column")[0]);
	for (var i = 0; i < articles.length; i++) {
		var link = "";
		link = $(articles[i]).find(".articles__h2.entry-title a").attr("href");
		link = link ? link : location.href;
		$(articles[i]).find(".ciceron-translate").length < 1
			&& $(articles[i]).find(".articles__summary-footer, .PostFull__time_author_category_large").append($(`<span class="ciceron-translate DropdownMenu" link="${link}">
						<a>
							<span>${l10n[user.locale].Translate}</span>
						</a>
						<form id="cp${i}" class="ciceron-translate-popup VerticalMenu menu vertical" target="_blank" action="${url}/request" method="GET">
							<table>
								<tr>
									<td>${l10n[user.locale].SourceLanguage}</td>
									<td class="ciceron-language">
										<div class="LoadingIndicator circle">
											<div></div>
										</div>
									</td>
								</tr>
								<tr>
									<td>${l10n[user.locale].TargetLanguage}</td>
									<td>
										<select name="transLang">
											<option value="KO">${getLanguageNameFromKey("ko")}</option>
											<option value="EN">${getLanguageNameFromKey("en")}</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>${l10n[user.locale].ExpectedCost}<!--<a href="${url}/help" target="_blank"><img class="ciceron-help" src="${chrome.extension.getURL('q.png')}" /></a>--></td>
									<td class="ciceron-price">
										<div class="LoadingIndicator circle">
											<div></div>
										</div>
									</td>
								</tr>
							</table>
							<input type="hidden" name="link" value="${link}" />
							<input type="hidden" name="locale" value="${user.locale}" />
							<input type="hidden" name="steemitId" value="${user.account}" />
							<button class="ciceron-button" type="submit">${l10n[user.locale].Next}＞</button>
							<p style="width: 100%; height: auto; position: relative; text-align: center; margin-top: 12px; margin-bottom: 0; font-size: .8em; font-weight: 100;"> 
								Powered by CICÉRON
							</p>
						</form>
						
					</span>`));
	}
};


// $(".PostFull__footer .column").append($(`<span class="ciceron-translate">
// <a href="#">${l10n.translate}</a>
// </span>`));

var lastClickedLink = "";

$("body").on("click", ".ciceron-translate", function (e) {
	var clickedObject = this;
	setTimeout(() => {
		$(clickedObject).addClass("show");
		if (lastClickedLink != $(clickedObject).attr("link")) {
			lastClickedLink = $(clickedObject).attr("link");
			var formData = new FormData();
			formData.append("link", lastClickedLink);

			$.ajax({
				url: url + "/api/v1/trans/poking",
				processData: false,
				contentType: false,
				dataType: "JSON",
				data: formData,
				cache: false,
				type: "POST"
			}).done(function (data) {
				// console.log(data);
				// console.log($(clickedObject).find(".ciceron-language"));
				$(clickedObject).find(".ciceron-language").html(getLanguageNameFromKey(data.originLang));
				$(clickedObject).find(".ciceron-price").html(data.price + " STEEM");
			}).fail(function (e, w, q) {
				console.log(e);
				console.log(w);
				console.log(q);

			});
		}
	}, 1);
});

$("body").on("click", function (e) {
	$(e.target).hasClass("ciceron-translate-popup") || $(".ciceron-translate").removeClass("show");
});



var getUserAccountAndInit = function () {
	var domHeader = $(".Header__userpic");
	var checkHeaderExist = () => {
		return domHeader.length > 0
	};

	if (!checkHeaderExist()) {
		setTimeout(() => {
			getUserAccountAndInit();
		}, 10);
		return;
	}

	var userAccount = domHeader.find("a").attr("title");
	user.account = userAccount;
	document.addEventListener('DOMNodeInserted', nodeInsertedCallback);
	nodeInsertedCallback(null);
}


$.ajax({
	url: 'https://steemit.com/',
	processData: false,
	contentType: false,
	dataType: "html",
	type: 'GET'
}).done(function (data) {
	var s = $(data).find("script");
	for (var i = 0; i < s.length; i++) {
		try {
			var u = JSON.parse(s[i].innerHTML);
			// if (u.offchain && u.offchain.account) {
			// 	// console.log("계정 이름: ", u.offchain.account);
			// 	user.account = u.offchain.account;
			// 	$("input[name='steemitId']").val(user.account);
			// }
			if (u && u.app && u.app.user_preferences && u.app.user_preferences.locale) {
				user.locale = u.app.user_preferences.locale;
				console.log(u.app.user_preferences.locale);
				break;
				// console.log(user.locale);
			}
		}
		catch (e) {

		}
	}
	getUserAccountAndInit();
}).fail(function (e, w, q) {
	console.log(e);
	console.log(w);
	console.log(q);
});
